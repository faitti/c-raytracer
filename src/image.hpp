#pragma once
#include "vec.hpp"
#include <iostream>
#include <fstream>

class Image {
    private:
        std::ofstream file;
        int height;

    public:
        void NewImage(int w, int h);
        void WriteToImage(color v);
        void CloseFile();
        void WriteProgress(int p);
};
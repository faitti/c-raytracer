#include "sphere.hpp"

double Sphere::intersect(const point& c, double r, const ray& ray) {
    vec orig_center = ray.origin() - c;
    auto dot = ray.direction().squared_len();
    auto dotb = dotp(orig_center, ray.direction());
    auto dotc = orig_center.squared_len() - r * r;
    auto disc = dotb * dotb - dot * dotc;

    if (0 > disc) { return -1.0; }
    return (- dotb - std::sqrt(disc)) / (2.0 * dot);
}
#include "image.hpp"

void Image::NewImage(int w, int h) {
    height = h;
    file.open("output.ppm");
    file << "P3\n" << w << " " << h << "\n255\n";
}

void Image::WriteToImage(color v) {
    int rr = static_cast<int>(255.999 * v.x());
    int gg = static_cast<int>(255.999 * v.y());
    int bb = static_cast<int>(255.999 * v.z());

    file << rr << " " << gg << " " << bb << "\n";
}

void Image::CloseFile() {
    file.close();
}

void Image::WriteProgress(int p) {
    std::cout << "\rProgress: " << ((float)(height - p) / (float)(height) * 100) << "%" << std::flush;
}
#pragma once
#include "vec.hpp"
#include "ray.hpp"

namespace Sphere {
    double intersect(const point& c, double r, const ray& ray);
};
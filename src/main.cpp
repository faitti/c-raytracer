#include "image.hpp"
#include "vec.hpp"
#include "ray.hpp"
#include "sphere.hpp"

color ray_c(const ray& r) {
    auto hit = (Sphere::intersect(point(0, 0, -1), 0.5, r));
    if (hit > 0.0) {
        vec norm = unit_vec(r.at(hit) - vec(0, 0, -1));
        return 0.5 * color(norm.x() + 1, norm.y() + 1, norm.z() + 1);
    }
    vec unit_dir = unit_vec(r.direction());
    double j = 0.5 * (unit_dir.y() + 1.0);
    return (1.0 - j) * color(1.0, 1.0, 1.0) + j * color(0.5, 0.7, 1.0);
}

int main(int argc, char** argv) {
    const double aspect = 16.0 / 9.0;
    const int w = 512;
    const int h = static_cast<int>(w / aspect);

    double view_h = 2.0;
    double view_w = aspect * view_h;
    double f_len = 1.0;

    point orig = point(0, 0, 0);
    vec horiz = vec(view_w, 0, 0);
    vec vert = vec(0, view_h, 0);
    vec l_corner = orig - horiz /2 - vert/2 - vec(0, 0, f_len);

    Image* img = new Image();
    img->NewImage(w, h);
    for (int j = h-1; j >= 0; --j) {
        img->WriteProgress(j);
        for (int k = 0; k < w; ++k) {
            double u = double(k) / (w - 1);
            double v = double(j) / (h - 1);
            ray r(orig, l_corner + u * horiz + v * vert);
            color pix = ray_c(r);
            img->WriteToImage(pix);
        }
    }
    img->CloseFile();
    return 0;
}
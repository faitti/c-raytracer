#pragma once
#include <cmath>
#include <iostream>
#include <fstream>

class vec {
    public:
        vec(): e{0, 0, 0} {}
        vec(double x, double y, double z): e{x, y, z} {}

        double x() const { 
            return e[0]; 
        }

        double y() const { 
            return e[1]; 
        }

        double z() const { 
            return e[2]; 
        }

        double operator[] (int j) const {
            return e[j];
        }

        double& operator[] (int j) {
            return e[j];
        }

        vec operator - () const {
            return vec(-e[0], -e[1], -e[2]);
        }

        vec& operator *= (const double j) {
            e[0] *= j;
            e[1] *= j;
            e[2] *= j;
            return *this;
        }

        vec& operator += (const vec &v) {
            e[0] += v.e[0];
            e[1] += v.e[1];
            e[2] += v.e[2];
            return *this;
        }

        vec& operator /= (const double j) {
            return *this *= 1/j;
        }

        double len() const {
            return std::sqrt(squared_len());
        }

        double squared_len() const {
            return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
        }

        double e[3];
};

using point = vec;
using color = vec;

inline std::ostream& operator<<(std::ostream &out, const vec &v) {
    return out << v.e[0] << ' ' << v.e[1] << ' ' << v.e[2];
}

inline vec operator + (const vec &u, const vec &v) {
    return vec(u.e[0] + v.e[0], u.e[1] + v.e[1], u.e[2] + v.e[2]);
}

inline vec operator - (const vec &u, const vec &v) {
    return vec(u.e[0] - v.e[0], u.e[1] - v.e[1], u.e[2] - v.e[2]);
}

inline vec operator * (const vec &u, const vec &v) {
    return vec(u.e[0] * v.e[0], u.e[1] * v.e[1], u.e[2] * v.e[2]);
}

inline vec operator * (double t, const vec &v) {
    return vec(t*v.e[0], t*v.e[1], t*v.e[2]);
}

inline vec operator * (const vec &v, double t) {
    return t * v;
}

inline vec operator / (vec v, double t) {
    return (1/t) * v;
}

inline double dotp(const vec &u, const vec &v) {
    return u.e[0] * v.e[0] + u.e[1] * v.e[1] + u.e[2] * v.e[2];
}

inline vec crossp(const vec &u, const vec &v) {
    return vec(
        u.e[1] * v.e[2] - u.e[2] * v.e[1],
        u.e[2] * v.e[0] - u.e[0] * v.e[2],
        u.e[0] * v.e[1] - u.e[1] * v.e[0]
    );
}

inline vec unit_vec(vec v) {
    return v / v.len();
}

#pragma once
#include "vec.hpp"

class ray {
    public:
        ray() {}
        ray(const point& origin, const vec& direc): orig(origin), dir(direc) {}

        vec direction() const {
            return dir;
        }

        point origin() const {
            return orig;
        }

        point at (double j) const {
            return orig + j * dir;
        }

        point orig;
        vec dir;
};
.PHONY: src

src:
	cd ./src && \
	g++ *.cpp -o main \
	-Wall -Ofast -fconcepts-ts && \
	mv ./main ../build

clean:
	rm build
	echo "Build removed"